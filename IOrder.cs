﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntefaceAbstract
{
    public interface IOrder
    {
        void PlaceOrder();
        void CancelOrder();
        void GetOrder();
    }
}
