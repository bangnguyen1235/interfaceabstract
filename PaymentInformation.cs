﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntefaceAbstract
{
    public abstract class PaymentInformation
    {
        private int _paymentNo = 10;
        public virtual int PaymentNo() { return _paymentNo; }
    }
}
