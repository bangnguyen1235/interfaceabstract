﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntefaceAbstract
{
    public class PizzaOrder : PaymentInformation, IOrder
    {
        private int _price = 50000;
        private int _amount = 0;
        private bool _isOrdered = false;
        public int Amount { get { return _amount; } set { _amount = value; } }          
        public PizzaOrder(int amount) {          
            _amount = amount;            
        }
        public PizzaOrder() : base(){ 
            
        }
        public override int PaymentNo()
        {
            return base.PaymentNo();
        }
        public void CancelOrder()
        {
            if (_isOrdered == true) {
                _isOrdered = false;
                Console.WriteLine("Cancle Ordered Successfully ");
            }
            else Console.WriteLine("Not ordered");
        }

        public void GetOrder()
        {
            _isOrdered = true;
            Console.WriteLine(PaymentNo() + " " + _price + " " + _amount + " "  + _isOrdered);
        }

        public void PlaceOrder()
        {
            if (Amount == 0) {
                Console.WriteLine("Amount must be greater than 0");
            }
            else if (_isOrdered == true)
                Console.WriteLine("Place Ordered");
            else
            {
                _isOrdered = true;
                Console.WriteLine(PaymentNo() + " Total: " + _price * Amount);
            }
                
        }
    }
}
