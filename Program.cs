﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntefaceAbstract
{
    internal class Program
    {
        static void Main(string[] args)
        {
            PizzaOrder pizzaOrder = new PizzaOrder(0);

            //pizzaOrder.CancelOrder();
            pizzaOrder.PlaceOrder();

            Console.ReadKey();

        }
    }
}
